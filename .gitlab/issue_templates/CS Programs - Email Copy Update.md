<!--Purpose of this issue: To request an update to a digital program in Gainsight.-->

## Submitter Checklist
* **Please note that for a new program there is a specific issue template to use.**
- [ ] Name this issue `Program Copy Update: <brief description of update>` <!--ex. Program Copy Update: Update links in onboarding)-->
- [ ] What needs to be removed, updated and/or added?
- [ ] Reasoning:
- [ ] What programs are affected by this change? [Reference handbook page](https://about.gitlab.com/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/#digital-customer-programs)
   - In suggestion/comment mode please apply changes on relevant copy docs found on the handbook page
- [ ] Is there custom field tokenization involved?
   - If yes, what is the correct field name?
   - Gainsight or SFDC?
- [ ] Desired completion Date: 
- Who needs to provide feedback and sign off before closing issue?

## Progress Checklist
1. [ ] Confirm that required details were provided and issue is ready to be worked.
1. [ ] Apply update to email(s) in Gainsight
1. [ ] Comment that the copy has been updated
1. [ ] Close this issue.
<!-- Please leave the label below on this issue -->
/label ~CSOps ~"CS Programs"
/cc @llandon @mharris3 
