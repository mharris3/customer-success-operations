<!--This issue template is designed to help you request a customer webinar event. Title this issue with `Program Webinar Request: [Title]`.-->

<!--Note: Please allow 10 business days from the date of this request to the first email invitation for the webinar.-->

**Proposed Launch Timeline**: <!--Program Manager to add when dates are finalized-->

| Date | Webinar Name | Invite 1  |  Reminder | Follow-Up / Survey | Registration Link  |
|-----------------|:-------------|:---------------:|---------------:|---------------:|---------------:|
| --- | ---  | --- |  --- | --- | --- |
---

**Note**: Please allow _10_ business days from completion of *all* coordinator checklist items for this request to the first email invitation for the webinar.

## {-Pre-Event Tasks-}

- [ ] Assign event coordinator: <!--This is the person who will be either running the webinar or coordinating it.--> `@Person name`

### Coordinator checklist <!--Failure to complete these fields may cause a delay in the setup of the webinar-->

1. [ ] Assign Roles<!--Add the @names of each person involved in the webinar-->
   1. Presenter: <!--Event Coordinator to add--> `@Person name`
   1. Webinar coordinator (to assist with chat and gate at the event): <!--Event Coordinator to add--> `@Person name`
   1. Q&A assistance: <!--Add the @names of each person helping with Q&A--> `@Person name`
1. [ ] Set webinar date (Eastern timezone/UTC) - <!--Event Coordinator to add--> `Webinar date`
1. [ ] Webinar title - <!--Event Coordinator to add. For example, "Intro to GitLab"--> `Webinar title`
1. [ ] Webinar landing page description - <!--Event Coordinator to add and Technical Writing to review. For example, "This webinar will help you learn how to use GitLab quickly, and get started with a solid foundation."--> Add the landing page description to the [copy doc below](#twc).
1. [ ] Webinar slide deck or recording - <!--Please link to the recording or slide deck that will be used for the webinar.-->: `Webinar recording or slide deck link`
1. [ ] Recipient information: <!--Please provide a .CSV file if applicable. Otherwise, describe the recipients for this program. For example, Net New customers within the last 30 days, AMER, have completed onboarding, Gainsight Admin and Web Direct--> `Recipient criteria`

### <a name="twc"></a> Technical writing checklist

- [ ] Review | Update (if needed) | Approve landing page description
- [ ] Create email invitation template in Gainsight | Reminder - [Use this template and link your document below](https://docs.google.com/document/d/1llWNOY3oPudSWi2fHxX2JY7VD9DsDtu0LGpF_QjfB8I/copy)

<!-- Link document here -->

### Program manager checklist

- [ ] Confirm that required details were provided and issue is ready to be worked.
- [ ] Verify provided participant criteria.
- [ ] Create digital program.
- [ ] Share email template for TAM/AE promotion in Gainsight (Outreach, Email Assist, Gmail).
- [ ] Coordinate with Marketing for GitLab social media promo post, if applicable.
- [ ] Add to monthly release newsletter if there is time.
- [ ] Comment that the program invite is scheduled for deployment (include date and time).
- [ ] Create webinar in Zoom - set up recording to cloud [(instructions here)](https://gitlab.zoom.us/rec/share/-NswsUyW4aHx-EzZj9SVyAwKfijhBe1LHmnm9KUuYzEn51AO_Sp6XRao0ZSlR96e.9Y7acbIVObjQpmY5).

## {-Post-Event Tasks-}

### Technical writing checklist

- [ ] Create email follow-up templates in Gainsight, with webinar recording (YouTube), Q&A (PDF), and any other links provided by event coordinator (use existing copy doc).

### Program manager checklist

- [ ] Upload Zoom recording to YouTube > GitLab Unfiltered page > Webinars playlist. Expected completion - within 1 business day.
- [ ] Create digital program for follow-up emails. Expected completion - within 1 business day, barring unforeseen circumstances.
- [ ] Comment that the program follow-up email is scheduled for deployment (including date and time).
- [ ] Share details of post-campaign analysis. Expected completion - within 10 business days.

---

<!--*Do not edit below this line*<-->
/label ~"CSOps" ~"CSOps::New" ~"CS Programs" ~"CS Ops Technical Writing"
/assign @llandon
/confidential
cc: @mharris3 @llandon @rgorbanescu @johnpgamboa 
