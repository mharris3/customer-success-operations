<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE CS OPS TEAM WILL REVIEW AND ASSIGN ON OUR END -->

## Background
<!-- Please summarize the purpose of this request. -->



### Requirements
<!-- Please describe, in detail, the scope of this request (fields, filters, timeframe, platform - gainsight or sisense, etc) -->



### Business Justification
<!-- What will this help solve or accomplish? -->



## Specifications
<!-- Leave blank. For CS Operations notes -->

### Resolution
* [ ] Provide a summary of what was done to complete this issue. List the names and links of any items that were changed/created/deleted (reports, dashboards, rules, etc.).
* [ ] If applicable, update handbook documentation and link to MR here:
* [ ] If applicable, add to [Gainsight changelog](https://docs.google.com/spreadsheets/d/1QknfSfX50JFOhkHZoz2VjovREWK2fJkYBxr5HEjkT9Q/edit#gid=0)
* [ ] If applicable, follow the [peer review process](https://about.gitlab.com/handbook/sales/field-operations/customer-success-operations/#peer-review)
* [ ] Close out issue and celebrate!

<!-- Please leave the labels below on this issue -->
/label ~"CSOps" ~"CS product usage reporting"
cc @nk312 @bbutterfield 
