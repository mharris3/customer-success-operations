# Key Information

1. For which system(s) is this change/update for? (select all that apply)
- [ ] Sisense
- [ ] Salesforce
- [ ] Troops
- [ ] Other <!-- Please specify -->


2. What specific dashboard(s)/report(s) should be changed? (please list the full title as it appears in the report/dashboard)
- [Enter Dashboard Title Here](paste dashboard URL within the parenthesis) 
- [Enter Report Title Here](paste report URL within the parenthesis if applicable)

3. What needs to be changed/updated?

<!-- Please explain what needs to be changed/updated and include any specific field names or screenshots if possible -->



4. What is the reasoning for this change?

<!-- Why does this change/update need to happen? What does this improve? etc. -->



5. Is this change specific to one person/team or should this change be applied for other people/teams as well? 
- [ ] One person/team
- [ ] Should be changed for other people/teams <!-- please tag any others that may benefit -->



<!-- Below this line is for CS Ops only -->

# Technical Requirements 

<!-- CS Ops to complete this section -->



<!-- Label and assignment section -->
/label ~"CSOps" ~"CSAnalytics" ~"CSOps - SA"
/epic gitlab-com/sales-team/field-operations&160 
cc @bbutterfield
