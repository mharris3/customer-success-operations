<!--Purpose of this issue: To request an update to a digital program in Gainsight.-->

## Submitter Checklist
* **Please note that for a new program there is a specific issue template to use.**
- [ ] Name this issue `Program Participant Update: <brief description of update>` <!--ex. Expanding SMB Post Churn survey to MM)-->
- [ ] What needs to be removed, updated and/or added in the program participant logic?
- [ ] Reasoning:
- [ ] What programs are affected by this change? [Reference handbook page](https://about.gitlab.com/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/#digital-customer-programs)
- [ ] Can you reference any Gainsight or SFDC fields?
- [ ] Desired completion Date: 
- Who needs to provide feedback and sign off before closing issue?

## Progress Checklist
1. [ ] Confirm that required details were provided and issue is ready to be worked.
1. [ ] Apply update to program participant logic in Gainsight
1. [ ] Comment that update has been made
1. [ ] Close this issue.
<!-- Please leave the label below on this issue -->
/label ~CSOps ~"CS Programs"
/cc @mharris3 @johnpgamboa

