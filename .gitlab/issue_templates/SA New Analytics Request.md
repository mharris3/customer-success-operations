<!-- Please fill out all the information below the best you can -->

## Business Requirement

<!-- 
Please tell us the business problem you are trying to solve or the request that you have, the more detail the better!

A good starting point is: Why is this important? How does this help? What problem does this solve? 

Attach any screenshots, diagrams, or links related to the Business Requirements. 
-->



## Technical Requirements

What type of analytics request is this?
- [ ] Sisense Dashboard
- [ ] Sisense Report
- [ ] Salesforce Dashboard
- [ ] Salesforce Report
- [ ] Ad-hoc Analysis
- [ ] Unsure
- [ ] Other <!-- Please describe what "other" is -->


What needs to be built?

<!-- Please add any detail not included above of what needs to be built including specific field names, time frames, etc. -->



What filters should be applied?

<!-- List any and all filters you would like applied to the analytics such as region, date range, segment, etc. -->



Is this report only for you/your team or would other people/teams benefit from this as well?
- [ ] Only for me/my team
- [ ] Others could benefit <!-- please tag any others that may benefit -->



## CS Ops Notes

<!-- CS Ops to complete this section -->



<!-- Label and assignment section -->
/label ~"CSOps" ~"CSAnalytics" ~"CSOps - SA"
/epic gitlab-com/sales-team/field-operations&160
cc @bbutterfield
