<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE CUSTOMER SUCCESS OPS TEAM WILL REVIEW AND ASSIGN ON OUR END -->

## Requirements | Request
<!-- Please tell us the business problem you are trying to solve or the request that you have, the more detail the better -->
<!-- Attach any screenshots, diagrams or links related to the Business Requirements -->



## Intended Outcome
<!-- Please describe the expected outcome of this if successful. What do you want to result from this issue? -->



## Specifications
<!-- Leave blank. For Sales Operations notes -->

* [ ] Once project is complete, communicate to relevant parties
* [ ] Peer Review process performed, if applicable
* [ ] Add to [Gainsight changelog](https://docs.google.com/spreadsheets/d/1QknfSfX50JFOhkHZoz2VjovREWK2fJkYBxr5HEjkT9Q/edit#gid=0)
* [ ] If applicable, complete change management process (Enablement documentation, training, and/or videos)
* [ ] If applicable, update handbook documentation



<!-- Please leave the label below on this issue -->
/label ~CSOps 


