<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE CS OPS TEAM WILL REVIEW AND ASSIGN ON OUR END -->

## Issue
<!-- Please summarize the problem you are encountering. -->



### Expected behavior
<!-- Please describe, in detail, what you expected to happen in this scenario. -->



### Observed behavior
<!-- Please describe, in detail, what you observed happen. Provide as much information as possible, including any applicable screenshots or recordings. -->



## Specifications
<!-- Leave blank. For CS Operations notes -->

### Resolution
* [ ] Provide a summary of what was done to resolve this issue. List the names and links of any items that were changed/created/deleted (reports, dashboards, rules, etc.).
* [ ] If applicable, update handbook documentation and link to MR here:
* [ ] Add to [Gainsight changelog](https://docs.google.com/spreadsheets/d/1QknfSfX50JFOhkHZoz2VjovREWK2fJkYBxr5HEjkT9Q/edit#gid=0)
* [ ] If applicable, follow the [peer review process](https://about.gitlab.com/handbook/sales/field-operations/customer-success-operations/#peer-review)
* [ ] Close out issue and celebrate!

<!-- Please leave the labels below on this issue -->
/label ~"CSOps" ~"gainsight" ~"gainsight::bug" ~bug 

