<!-- Please fill out all the information below the best you can -->

# Key Information

1. In which system(s) is this issue being noticed? (select all that apply)
- [ ] Sisense
- [ ] Salesforce
- [ ] Troops
- [ ] Other <!-- Please specify -->


2. What specific dashboard(s)/report(s) are experiencing this issue? (please list the full title as it appears in the report/dashboard)
- [Enter Dashboard Title Here](paste dashboard URL within the parenthesis) 
- [Enter Report Title Here](paste report URL within the parenthesis if applicable)


3. What filters are being applied when this issue is occurring? (please include the filter name(s) and the values of the filter(s) a screenshot of the filter(s) will suffice)
- `Filter Name` = `Filter Value`
- `Filter Name` = `Filter Value`


4. Is a different value/result being seen in a different system/report/dashboard?
- [ ] Yes
- [ ] No


5. Which system/report/dashboard is showing a different value/result?
- [ ] N/A
- [ ] `system/report/dashboard Name`


6. If you are expecting a different value/result please provide what you would expect the value to be if possible.
- `Expected Value/Result`



## Description

<!-- Please describe the issue as best you can beyond what was filled out above -->



## Screenshots

<!-- Please add any screenshots that show the filters being applied, where the issue is happening, what the issue looks like, or what the expected value/result should be. -->



<!-- Below this line is for CS Ops only -->

# Technical Requirements 

<!-- CS Ops to complete this section -->



<!-- Label and assignment section -->
/label ~"CSOps" ~"CSAnalytics" ~"CSOps - SA"
/epic gitlab-com/sales-team/field-operations&160
cc @bbutterfield
