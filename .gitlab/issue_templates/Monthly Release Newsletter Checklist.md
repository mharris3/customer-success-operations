# Monthly Release Newsletter Checklist

These are the components required to gather all the necessary information for the release newsletter sent each month.

<!--Add the month and year of the newsletter to the issue title. Example: `October 2021 monthly release newsletter checklist`-->

## Timing

- We begin putting together the newsletter each month, following the release on the 22nd.
- We send the newsletter on a Friday, usually in the second week of the month.
- TMM or other groups provide the videos within 10-14 days of the release.

---

## Gather newsletter information

- [ ] Add videos from Technical Marketing team. Their team creates a [related issue to see progress](https://gitlab.com/groups/gitlab-com/marketing/strategic-marketing/-/epics/396).
- [ ] Add a GitLab Learn link to the email.
- [ ] Add any webinars or other monthly events when applicable.
- [ ] Add any additional callouts from other teams.
- [ ] Create first draft of [email copy](https://docs.google.com/document/d/1VkN-pqElJJtqz2vsLySnm5FDTsHVK7wyZwX_97WeyOM/edit#).

---

### Get input from other teams

- [ ] Request input from associated teams in Slack:
  -  #tam-updates
  -  #commercial_global_all
  -  Other teams as needed

## Review
  ---

- [ ] Complete review, check for typos, grammar, style.
- [ ] Post in the appropriate Slack channels with link to screenshot of email/issue.

  ---

## Gainsight program setup

1. [ ] Clone previous month's email template, renaming as needed.
1. [ ] Copy/paste content from GDocs into Notes to remove formatting, then into the new template.
1. [ ] Change subject line.
1. [ ] Create tokens within the template.
1. [ ] Edit links and add link tracking.
1. [ ] (Optional) Create additional templates as needed. Request Ops review if new templates are created.
1. [ ] In Programs - Edit the existing newsletter programs - TAM, and Digital/Web Direct.
1. [ ] Select the new template from the dropdown menu.
1. [ ] Check the recipient list for errors or additions.
1. [ ] Sync the recipient information. This can take up to 30 minutes.
1. [ ] Send test when sync is complete. This will show a real tokenized customer name and/or TAM name.
1. [ ] Test all links.
1. [ ] Create a screenshot of the email, either in the test email or Gainsight. Add it to the issue.
1. [ ] Schedule the email to send at 10:00am on the due date.

---

## Post-launch

1. [ ] (Within 5 days) Get Gainsight analytics for program, including open rate, click rate, and most-clicked link information.
1. [ ] Add analytics screenshots to issue.
1. [ ] Disable program.
1. [ ] Close issue.

<!--Do not edit below this line.-->

/epic https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/85

/label ~CSOps ~"CSOps::New" ~"CS Programs" ~"CS Ops Technical Writing::In Progress"
/assign me

cc: @llandon @mharris3
