<!--This issue template is designed to help you request a customer webinar event. Title this issue with `Program Webinar Request: [Title]`.-->

<!--Note: Please allow 10 business days from the date of this request to the first email invitation for the webinar.-->

**Proposed Launch Timeline**: <!--Program Manager to add when dates are finalized-->

| Date | Webinar Name | Invite 1  |  Reminder | Follow-Up / Survey | Registration Link  |
|-----------------|:-------------|:---------------:|---------------:|---------------:|---------------:|
| --- | ---  | --- |  --- | --- | --- |
---

**Note**: Please allow _10_ business days from completion of *all* coordinator checklist items for this request to the first email invitation for the webinar.

## {-Pre-Event Tasks-}

- [ ] Assign event coordinator: <!--This is the person who will be either running the webinar or coordinating it.--> `@Person name`

### Coordinator checklist <!--Failure to complete these fields may cause a delay in the setup of the webinar-->

1. [ ] Assign Roles<!--Add the @names of each person involved in the webinar-->
   1. Presenter: <!--Event Coordinator to add--> `@Person name`
   1. Webinar coordinator (to assist with chat and gate at the event): <!--Event Coordinator to add--> `@Person name`
   1. Q&A assistance: <!--Add the @names of each person helping with Q&A--> `@Person name`
   1. Indicate if the webinar is live or based on previous recording: <!--Event Coordinator to add--> `Live or Previously Recorded`
1. [ ] Set webinar date (Eastern timezone/UTC) - <!--Event Coordinator to add--> `Webinar date`
1. [ ] Webinar title - <!--Event Coordinator to add. For example, "Intro to GitLab"--> `Webinar title`
1. [ ] Webinar landing page description - <!--Event Coordinator to add and Technical Writing to review. For example, "This webinar will help you learn how to use GitLab quickly, and get started with a solid foundation."--> Add the landing page description to the [copy doc below](#twc).
1. [ ] Webinar slide deck or recording - <!--Please link to the recording or slide deck that will be used for the webinar.-->: `Webinar recording or slide deck link`
1. [ ] Recipient information: <!--Please provide a .CSV file if applicable. Otherwise, describe the recipients for this program. For example, Net New customers within the last 30 days, AMER, have completed onboarding, Gainsight Admin and Web Direct--> `Recipient criteria`

### <a name="twc"></a> Technical writing checklist

- [ ] Review | Update (if needed) | Approve landing page description
- [ ] For new topics, create email invitation template in Gainsight | Reminder - [Clone this document and link here](https://docs.google.com/document/d/1vZKF3Si98oRfa266EOTsiL1nLVCm38st6DGki-FP8m8/edit#heading=h.z2typ65ysmf6)
- [ ] For new topics, create follow up email template in Gainsight 
- [ ] Add to monthly release newsletter if there is time.

### Program manager checklist

- [ ] Confirm that required details were provided and issue is ready to be worked.
- [ ] Verify provided participant criteria.
- [ ] Create webinar in Zoom - set up recording to cloud [(instructions here)](https://gitlab.zoom.us/rec/share/-NswsUyW4aHx-EzZj9SVyAwKfijhBe1LHmnm9KUuYzEn51AO_Sp6XRao0ZSlR96e.9Y7acbIVObjQpmY5).
- [ ] Create digital program 
- [ ] Comment that the program invite is scheduled for deployment (include date and time).


## {-Post-Event Tasks-}

### Technical writing checklist

- [ ] Create email follow-up templates in Gainsight, with webinar recording (YouTube), Q&A (PDF), and any other links provided by event coordinator (use existing copy doc).

### Program manager checklist

- [ ] Remove webinar from [Webinar Handbook page](https://about.gitlab.com/handbook/customer-success/tam/segment/scale/webinar-calendar/) 
- [ ] Generate attendee information from Zoom Webinar
- [ ] Append Account ID, Account Name, TAM and AE Names to Zoom attendee report
- [ ] Add list of attended and registered participants to [this doc](https://docs.google.com/spreadsheets/d/11BwpZHioF_jJ-ZTiCGHAYKBNgV2XDM5i-lUUKD7rHWQ/edit#gid=582009751)
- [ ] Upload Zoom recording to YouTube > GitLab Unfiltered page > Webinars playlist. Expected completion - within 1 business day.
- [ ] For existing topics, create email follow-up templates in Gainsight, with webinar recording (YouTube), and any other links provided by event coordinator (use existing copy doc). - within 1 business days
- [ ] Create digital program for follow-up emails. Expected completion - within 1 business day, barring unforeseen circumstances.
- [ ] Comment that the program follow-up email is scheduled for deployment (including date and time).
- [ ] Share details of post-campaign analysis. Expected completion - within 10 business days.

---

<!--*Do not edit below this line*<-->
/label ~"CSOps" ~"CSOps::Assigned" ~"CS Programs" ~"CS Ops Technical Writing" ~"CSOps - Scale"
/assign @llandon @rgorbanescu
/confidential
/epic &120
cc: @llandon  @rgorbanescu @taylorlund 
