# Customer Feedback Submission

*Please verify that all boxes have been checked before you submit this issue. Missing information may cause delays.*

## Submission details

- [ ] Customer company, name of person providing feedback, and any other identifying information: 
- [ ] Customer market segment. _For example, Commercial or SMB:_ 
- [ ] Source for feedback. _For example, direct feedback from a customer, NPS survey:_

## Feedback details

<!--Please enter the feedback here with as much detail as possible. If you have additional information about this issue, feedback, or customer, add it here as well.-->


<!--Do not edit below this line-->

/label ~CSOps ~"CSOps::New" ~"CS Programs" ~"CS Ops Technical Writing::Backlog"
/epic https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/82
cc: @llandon @mharris3
/assign @llandon
