# Create a new customer program

## Purpose

A brainstorming guide for SMEs to develop a new customer program. Please fill out this template as completely as possible.

## Assumptions about the target customer for the program

- Customer has decided to implement the stage or use-case. We are not selling them.
- Customer has sufficient time/resources assigned to work on this state/use-case.
- We don’t care what license level they have.  We will cover something for all levels, and they can choose to upgrade if needed.

### Step 1: Select the [Use Case](https://about.gitlab.com/handbook/use-cases/) or [Stage](https://about.gitlab.com/handbook/product/categories/) for the program.

*If your use case is not available, please select a stage*.

<details>
<summary>Use Case</summary>

<br>

- [ ] Version Control and Collaboration (VC&C)
- [ ] Continuous Integration (CI)
- [ ] Continuous Delivery (CD)
- [ ] DevSecOps (Shift Left Security)
- [ ] Agile Management
- [ ] GitOps
- [ ] DevOps Platform
- [ ] Cloud Native
- [ ] Other

</details>

<details>
<summary>Stage</summary>

<br>

- [ ] Manage
- [ ] Plan
- [ ] Create
- [ ] Ecosystem
- [ ] Verify
- [ ] Package
- [ ] Release
- [ ] Configure
- [ ] Monitor
- [ ] Secure
- [ ] Protect
- [ ] Other

</details>

<br>

### Step 2: Select the [Personas](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/) responsible for implementing

<details>
<summary>Personas</summary>

<br>

- [ ] Cameron
- [ ] Parker
- [ ] Delaney
- [ ] Presley
- [ ] Sasha
- [ ] Devon
- [ ] Sidney
- [ ] Sam
- [ ] Rachel
- [ ] Alex
- [ ] Simone
- [ ] Allison
- [ ] Priyanka
- [ ] Dakota

</details>

<br>

## Discovery questions

Use these discovery questions to ask what customers need for this use case/stage:

#### Basics:

- What are the prerequisites to implement this stage or feature?
- What are common problems or errors customers encounter when implementing this stage?  *(Typically ask CSMs and Support)*
- What are the top 5 features customers want to use in this stage or use case?

---

#### Customer requests:

- Where should I start?  What should I do first?
- What should I do second, third, etc?
- What are some of the basics I should know or focus on?
- What are some of the more advanced things I should know or do?

---

#### Training and other resources available:

- What are some variations of this stage (environment, project type, etc) that are not common to most/all customers?
- What docs pages cover this stage or use-case?
- What blog posts show how to implement this stage or use case?
- What YouTube videos (GitLab Unfiltered) show implementation side of this stage or use-case?
- What Learn platform tracks are available?
- What demo environment examples might be good to show a stage or borrow code snippets?

### Step 3: Answer the discovery questions

- [ ] Use the discovery questions to provide as much information about this stage or use case as possible.

---
<!-- Discovery answers go here -->



---

### Step 4: Assign this issue to a peer for review

- [ ] Assign this issue to **yourself** and another CSM, a CSM manager, or other SME to verify accuracy and provide answers to your questions.

- [ ] When your peer completes their review, create a new issue using the **CS Programs Request template**, and associate this issue with the new one.

---

**Do Not Edit Below This Line**

/label  ~"CSOps" ~"CS Ops Technical Writing"
cc: @mharris3 @llandon
