# Working Days :calendar: 
Please add a number for days you'll be working this milestone. We should subtract a day for each triage day and a day for meetings. For example, If a milestone is two weeks long and a person has 1 triage day she would subtract 2 triage days from the 10 workdays and then 1 meeting day as well, which would be 7 working days. All PTO should also be subtracted, including the covered triage days. For work that we send to Ky and Noha, we could just assign days for them based on how much they are contracted to do for us each Milestone. 

- @johnpgamboa: 
- @RGorbanescu: 
- @nk312:
- @bbutterfield:
- @cankney:
- @mharris3:

**Total working days**:  <!-- sum the above working days -->


**Estimated Total Issue Weight Capacity for Milestone**: <!-- sum the above working days multiplied by eight, then multiplied again by .60 -->


---

# Milestone Themes :book:

Prioritized issues to be completed as they relate to OKRs, Epics, or other [MVCs](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc). This provides us with a snapshot of the most important things we're planning to accomplish in the milestone for clear communication with the business as well as an easy measure for results.

We can use this to describe the general planning of the work we have for our Milestone. 

## Priority 1 -
* [Example] Resolve discrepancies between GCP data in bigquery and snowflake: https://gitlab.com/gitlab-data/analytics/-/issues/13062 [@ username involved]


## Priority 2- OKRs
* [Example] Build PH: Snowplow Loader (Data team) - data backfill implementation #13055 [@ username1 & @ username2]



## P3 - Other
---

* [Example] Update PtE model to work with the new Data Science project structure: https://gitlab.com/gitlab-data/analytics/-/issues/13347 [@ username1]


# Velocity and Capacity :race_car: 

We calculate the velocity of our last milestone by dividing the points/weight completed by the number of working days that were available.

## Last Milestone 
(fill out at Retrospective) and review at milestone launch

Last Milestone: <!-- link here (it will probably be the current milestone) -->
Last Milestone Issue: Not used. <!-- link here and also relate it in the linked issues section below-->
* **Last milestone weight completed:** <!-- from 'Last Milestone' linked above -->
* **Last milestone working days:** <!-- from 'Last Milestone Issue' linked above -->
* **Last milestone velocity:** <!-- 'Last milestone weight completed' / 'Last milestone working days' -->


## This (starting) Milestone: :crystal_ball: 
We assume that preceding milestone's velocity is a good predictor of the following milestone's velocity. We then make sure that we have adequate capacity to address all of the **prioritized issues**. If we don't have enough capacity then we remove the issues of least priority. 

**Capacity for this milestone:** <!-- Total days * Last milestone velocity -->


<!-- DO NOT EDIT BELOW THIS LINE -->

<!-- Please leave the label below on this issue -->

