<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE CUSTOMER SUCCESS OPS TEAM WILL REVIEW AND ASSIGN -->

<!--Purpose of this issue: To request a multi-email program to be sent via Gainsight.-->

## Requester checklist

**In order to complete a program request, _all_ of the information on this checklist must be provided**.
- [ ] Name of this program: <!--What name should this program have in Gainsight? Ex: DevSecOps Onboarding-->
- [ ] Purpose of this program: <!--What is the purpose, intent, and goal for this program? Ex: We may want to educate customers about a specific feature and provide in-depth knowledge.)-->
- [ ] Define success of the program: <!--How will we know if this program is successful? Ex: metrics, KPI, or other success measurements.-->
- [ ] Is this issue related to a larger / different initiative? <!--Link Epic / OKR, etc.-->
- [ ] What CSM segment is this for? (select all that apply)
   - [ ] Tech Touch
   - [ ] Scale
   - [ ] Mid touch
   - [ ] Strategic
- [ ] Who needs to provide feedback and sign off before closing this issue: <!--Tag appropriate person(s)-->
- [ ] Will this email require a dashboard, report or further analysis beyond standard email metrics (open / click rate)?

### Email logistical details: <!--Remove section if not requesting an email program-->

- [ ] Target send time: <!--What timezone are we sending to? Consider the participants region-->
- [ ] Target send date:
- [ ] Target customer list or csv upload: <!--Who are we trying to reach? Ex: Tech Touch customers who are 6 months into their contract and have low CI usage-->
- [ ] Create and link the email copy using the Google Doc [CS Ops technical writing email request template](https://docs.google.com/document/d/1llWNOY3oPudSWi2fHxX2JY7VD9DsDtu0LGpF_QjfB8I) and fill out the relevant information: <!--New document link here-->

<!--New document link here-->
- [ ] Suggested logic between email sends:<!--Ex:Wait 2 days before sending the 2nd email / create TAM CTA when customer responds to survey-->
 - [ ] Who is the email coming from: <!--GitLab Customer Success OR option to `token field` (For example, Technical Account Manager Name))-->
- [ ] Reply-to email: <!--This can be different than the from email address if you do not want customer responses. Ex: customer_enablementx@gitlab.com OR option to `token field`)-->

### For surveys: <!--Remove section if not requesting a survey-->

- [ ] Create a new Google Doc for survey copy using the [CS Ops Program Survey Questions Template](https://docs.google.com/document/d/1YzwmVhQZ5xLufCl9yBVwxLR3KNLwdhDQEdhUTQeeQvw) and fill out the relevant information. Add the link to your new document below.
- [ ] Create a new [Google Doc for the email portion of the survey](https://docs.google.com/document/d/1llWNOY3oPudSWi2fHxX2JY7VD9DsDtu0LGpF_QjfB8I) and add the link below.

<!--New survey document link here-->
<!--New survey email content document link here-->

## Program progress checklist
1. [ ] Program Manager: Confirm that required details were provided and issue is ready to be worked.
1. [ ] Program Manager: Create email, program and participant list in Gainsight.
1. [ ] Program Manager: Send to requester for final review and sign off.
1. [ ] Issue requester and reviewers: Provide approval, or updates in issue comment.
1. [ ] Program Manager: Comment that the issue is scheduled for deployment, including date and time.
1. [ ] Program Manager: Add email subjects and link copy doc (view only) to Handbook in digital journey, if applicable
1. [ ] Peer Review process performed, if applicable
1. [ ] Program Manager: Confirm that the program deployed successfully in the comments of the issue
1. [ ] Program Manager: Follow-up on performance metrics 24hrs OR open an issue for further analysis
1. [ ] Program Manager: Close this issue.

<!-- Please leave the label below on this issue -->
/label ~CSOps ~"CS Programs" ~"CS Ops Technical Writing"
/cc  @mharris3 @llandon @johnpgamboa
