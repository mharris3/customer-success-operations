<!-- Purpose of this issue: To suggest a webinar idea for TAM webinars. -->

## Submitter Checklist
* [ ] Name this issue `Webinar idea: <brief descriptive name>` (ex. Webinar Idea: Making the case for CI/CD in your organization)
* [ ] (Must read and check) Acknowledge that this is an idea for a webinar that we will discuss and likely ask questions to better understand the strategy and evaluate within priorities.
* [ ] Provide details requested in sections below, for any not filled in, please remove the helper text and DO NOT check the box in the list below.
   - [ ] Basic concept and goals
   - [ ] Potential speakers
   - [ ] Audience
   - [ ] Preliminary research
   - [ ] Timing

#### Basic concept and goals
<!-- Summarize the overall concept of your webinar. What is the goal of the webinar? What will attendees learn? Please share specific KPIs that your idea aims to improve, and ideally share the increase/decrease of that KPI that you are hoping to achieve. -->
Update here

#### Potential speakers
<!-- Do you have a specific speaker(s) in mind? Have they already expressed interest in participating? Please provide as much detail as possible and links as relevant. -->
Update here

#### Audience
<!-- Share the function (i.e. security, devops) and seniority (i.e. individual contributors, managers, C-level) as well as any other details you feel are relevant (i.e. users of a specific technology). If there is a geographic region, please indicate as well. Be as specific as possible! -->
Update here

#### Preliminary research
<!-- Please include any relevant links or commentary as to why this should be a focus in our demand generation horizon. -->
Update here

#### Timing
<!-- Is there a specific event or timing for which this campaign is time-based? If not, please indicate as such. -->
Update here



## Reviewer Checklist
* [ ] Program Manager: Confirm that required details were provided and explore any probing questions
* [ ] Discuss in CS Ops, and TAM team meetings
* [ ] Share reason for moving forward or closing with idea submitter in comments (and proceed with next steps accordingly)
* Close out this issue.


<!-- DO NOT UPDATE -
/label ~"CSOps" ~"CSOps::New" ~"CS Programs" ~"CS Ops Technical Writing"

-->
