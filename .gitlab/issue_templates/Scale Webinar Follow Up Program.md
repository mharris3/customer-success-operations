## {-What is this issue for?-}

This issue has been created for the purpose of tracking activities related to webinars that will be held in the month of `ADD MONTH HERE` 2022 and coordinated by the Scale CSE team. More specifically this issue will list all post-webinar tasks. There will be 6 such issues for October, one for each webinar held this month. All are linked for easy reference.

## {-Post-Event Tasks-}

### Program manager checklist

**Webinar Name and Date: ** " `ADD NAME OF WEBINAR HERE` ", hosted on `INSERT WEBINAR `

- [ ] Remove webinar from [Webinar Handbook page](https://about.gitlab.com/handbook/customer-success/tam/segment/scale/webinar-calendar/) 
- [ ] Generate attendee information from Zoom Webinar
- [ ] Append Account ID, Account Name, CSE and AE Names to Zoom attendee report
- [ ] Add list of attended and registered participants to [this doc](https://docs.google.com/spreadsheets/d/11BwpZHioF_jJ-ZTiCGHAYKBNgV2XDM5i-lUUKD7rHWQ/edit#gid=582009751)
- [ ] Upload Zoom recording to YouTube > GitLab Unfiltered page > Webinars playlist. Expected completion - within 1 business day.
- [ ] Create email follow-up templates in Gainsight, with webinar recording (YouTube), and any other links provided by event coordinator (use existing copy doc). - within 1 business days
- [ ] Create digital program for follow-up emails. Expected completion - within 1 business day, barring unforeseen circumstances.
- [ ] Comment that the program follow-up email is scheduled for deployment (including date and time).
- [ ] Share details of post-campaign analysis. Expected completion - within 10 business days.

 @taylorlund
