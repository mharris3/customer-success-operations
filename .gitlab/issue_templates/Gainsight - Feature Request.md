<!-- NOTE: PLEASE LEAVE THIS ISSUE UNASSIGNED - THE CS OPS TEAM WILL REVIEW AND ASSIGN ON OUR END -->

## Requirements | Request
<!-- Please tell us the problem you are trying to solve or the request that you have, the more detail the better -->
<!-- Attach any screenshots, diagrams or links related to the Requirements -->

## Specifications
<!-- Leave blank. For CS Operations notes -->

### Resolution
* [ ] Provide a summary of what was done to resolve this issue. List the names and links of any items that were changed/created/deleted (reports, dashboards, rules, etc.).
* [ ] If applicable, update handbook documentation and link to MR here:
* [ ] Add to [Gainsight changelog](https://docs.google.com/spreadsheets/d/1QknfSfX50JFOhkHZoz2VjovREWK2fJkYBxr5HEjkT9Q/edit#gid=0)
* [ ] If applicable, follow the [peer review process](https://about.gitlab.com/handbook/sales/field-operations/customer-success-operations/#peer-review)
* [ ] Close out issue and celebrate!

<!-- Please leave the labels below on this issue -->
/label ~"gainsight" ~"gainsight::feature_request" ~"CSOps"

